#ArtistFinder V1.8 by Arturo Mantinetti

from urllib.request import urlopen
from urllib.error import URLError
from datetime import date
import re

mainlink = "https://musicbrainz.org"
searchlink = "/search?query="
spacechar = "+"
endlink = "&type=artist&method=indexed"
lastyear = 10
singleSize = 10
onlastsong = False
find2 = False
lastfind = ""
timesfind = 0
maxtimefind = 3

def refind():
    global find2
    find2 = True

def setLastSingleSong(timeback,sizeSingles):
    global onlastsong
    global lastyear
    global singleSize
    onlastsong = True
    lastyear = timeback
    singleSize = sizeSingles

def searchArtist(artist):
    alldata = []
    for i in range(maxtimefind):
        link, error = find_max_score_link(artist,i)
        data = {}
        if (error!=0):
            data["error"] = error
            alldata.append(data)
            return alldata
        data = get_artist_data(link)
        data["pos"] = i + 1
        alldata.append(data)
        if(not find2):
            break
        if(len(data["albums"]["title"])!=0 and len(data["singles"]["title"])!=1 and data["singles"]["title"]!="None"):
            break
    return alldata

def make_find_link(artist):
    array = artist.split()
    link = mainlink + searchlink
    for i in range(len(array)):
        link += array[i]
        if (i<len(array)-1):
            link += spacechar
    link += endlink
    return link

def find_max_score_link(artist, n = 0):
    url = make_find_link(artist)
    error = 0
    #print("opening link =",url)
    try:
        response = urlopen(url)
    except URLError as e:
        error = e.reason
        return 0, error
    html = response.read()
    html = html.decode('utf-8')
    ###Verify if found anything
    isFoundAnything = re.findall(r'<div id="content">(.+)</div>', html, flags = re.DOTALL)
    tfisFound=isFoundAnything[0].find("No results found.")
    if (tfisFound>0):
        return 0, 1
    ##
    body = re.findall(r'<tbody>(.+)</tr>', html, flags = re.DOTALL)
    array_link = re.findall(r'a href="(.+)" title="', body[0])
    #print(array_link)
    link = mainlink + array_link[n]
    #print("link",link)
    return link, error

def get_artist_data(url):
    try:
        response = urlopen(url)
    except URLError as e:
        data = {}
        data["error"] = e.reason
        return data
    html = response.read()
    html = html.decode('utf-8')
    try:
        artistType = re.findall(r'<dd class="type">([^<]+)</dd>', html)[0]
    except:
        artistType = "None"
    #linecountry = re.findall(r'<dt>Area:</dt>(.+)<dt>ISNI code:</dt>', html, flags = re.DOTALL)
    linecountry = re.findall(r'<dt>Area:</dt>(.+)</dd>', html, flags = re.DOTALL)
    try:
        country = re.findall(r'<bdi>([^<]+)</bdi>', linecountry[0])
    except:
        country = []
        country.append("None")
    try:
        albums_html = re.findall('<h3>Album</h3>(.*?)</table>', html, re.DOTALL)
        album_years = re.findall(r' <td class="c">(.+)</td>', albums_html[0])
        album_title = re.findall(r'<bdi>(.*)</bdi>', albums_html[0])
    except:
        album_years = []
        album_years.append(0)
        album_title = []
        album_title.append("None")
    try:
        singles_html = re.findall(r'<h3>Single</h3>(.*?)</table>', html, re.DOTALL)
        single_years = re.findall(r' <td class="c">(.+)</td>', singles_html[0])
        findartist = singles_html[0].find('<th class="artist">Artist</th>')
        #print("findartist->", findartist)
        if (findartist==-1):#if (artistType == "Group"):
            #founded = re.findall(r'<dt>Founded:</dt>(?:.+)<dd>([^<]+)</dd>(?:.+)<dt>', html, flags = re.DOTALL)
            single_title = re.findall(r'<bdi>(.*)</bdi>', singles_html[0])
        else:
            #founded = re.findall(r'<dt>Born:</dt>(?:.+)<dd>([^<]+)</dd>(?:.+)<dt>', html, flags = re.DOTALL)
            Temp_single_title = re.findall(r'<bdi>(.*)</bdi></a>', singles_html[0])
            single_title = []
            for i in range(len(Temp_single_title)):
                if (i%2==0):
                    single_title.append(Temp_single_title[i])
    except:
        single_years = []
        single_years.append("----")
        single_title = []
        single_title.append("None")
    try:
        if (artistType == "Group"):
            founded = re.findall(r'<dt>Founded:</dt>(?:.+)<dd>([^<]+)</dd>(?:.+)<dt>', html, flags = re.DOTALL)
        elif(artistType == "Person"):
            founded = re.findall(r'<dt>Born:</dt>(?:.+)<dd>([^<]+)</dd>(?:.+)<dt>', html, flags = re.DOTALL)
        else:
            founded = re.findall(r'<dt>Founded:</dt>(?:.+)<dd>([^<]+)</dd>(?:.+)<dt>', html, flags = re.DOTALL)
            if (len(founded)==0):
                founded = re.findall(r'<dt>Born:</dt>(?:.+)<dd>([^<]+)</dd>(?:.+)<dt>', html, flags = re.DOTALL)
        if (len(founded)==0):
            founded.append("None")
    except:
        print("Exception")
        founded = []
        founded.append("None")
    ####
    if (onlastsong == False):
        for i in range (len(album_years)):
            album_years[i] = normalize(album_years[i])
            album_title[i] = normalize(album_title[i])
        for i in range (len(single_years)):
            single_years[i] = normalize(single_years[i])
            single_title[i] = normalize(single_title[i])
    ###
    youtubealt = re.findall(r'<li class="youtube-favicon">(.*)</a>', html, re.DOTALL)
    youtube = re.findall(r'<li class="youtube-favicon">\n          \n          \n          <a href="([^>]+)">', html)
    linkyoutube = []
    for a in youtube:
        linkyoutube.append("https:" + a)
    if(onlastsong):
        album_years, album_title, single_years, single_title = mindata(album_years,album_title,single_years,single_title)
    if (len(linkyoutube)==0):
        linkyoutube.append("None")
    #build Dictionary
    albums = {}
    albums["year"] = album_years
    albums["title"] = album_title
    singles = {}
    singles["year"] = single_years
    singles["title"] = single_title
    data = {}
    data["type"] = artistType
    data["founded"] = founded[0]
    data["country"] = country[0]
    data["albums"] = albums
    data["singles"] = singles
    data["youtube"] = linkyoutube
    data["error"] = 0
    return data

def normalize(text):
    if (isinstance( text, str)):
        text = text.replace("&amp;","&")
        text = text.replace("&#x2014;","----")
    return text

def mindata(album_years,album_title,single_years,single_title):
    minyear = date.today().year - lastyear
    newAlbum_y = []
    newAlbum_t = []
    newSingle_y = []
    newSingle_t = []
    albumSize = len(album_years)
    for i in range(albumSize):
        n = albumSize - i - 1
        album_title[n] = normalize(album_title[n])
        album_years[n] = normalize(album_years[n])
        if (album_years[n]=="----"):
            print("Advice -> None Year Find")
        elif(int(album_years[n])>=minyear):
            newAlbum_t.append(album_title[n])
            newAlbum_y.append(album_years[n])
        else:
            break
    for i in range(singleSize):
        n = len(single_years) - i - 1
        if (n>=0):
            single_title[n] = normalize(single_title[n])
            single_years[n] = normalize(single_years[n])
            newSingle_t.append(single_title[n])
            newSingle_y.append(single_years[n])
        else:
            break
    return newAlbum_y, newAlbum_t, newSingle_y, newSingle_t
