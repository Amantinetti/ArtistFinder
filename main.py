#ArtistFinder V1.8 by Arturo Mantinetti
#Main adapted to the DAA Exercise

from core import *

onrefind = False

def printdata(data,artist):
    if ((not onrefind) or data["pos"]==1):
        print("\n")
        print("Busqueda para:",artist)
    else:
        print("\t",data["pos"],"busqueda")
    if (data["type"]== "Group"):
        print("Fecha de Fundacion: \t",data["founded"][0:4])
    else:
        print("Fecha de Nacimiento: \t",data["founded"][0:10])
    print("Pais de Origen: \t", data["country"])
    print("Youtube Link: \t \t",data["youtube"][0])
    print("Albums:")
    print("\t   Año \t      Titulo")
    for i in range (len(data["albums"]["year"])):
        print("\t ",data["albums"]["year"][i],"\t",data["albums"]["title"][i])
    print("Singles:")
    print("\t   Año \t      Titulo")
    for i in range (len(data["singles"]["year"])):
        print("\t ",data["singles"]["year"][i],"\t  ",data["singles"]["title"][i])
    print("\n")

def printError(artist, error):
    print("\n")
    print("Busqueda para:",artist)
    #print("Error obtiendo los datos de:",artist)
    print("Error ->", error)
    print("\n")

def printNotFound(artist):
    print("\n")
    print("Busqueda para:",artist)
    #print("Error obtiendo los datos de:",artist)
    print("\t No se a encontrado el Artista")
    print("\n")

def argument(text):
    if (text=="-R"):
        refind()
        global onrefind
        onrefind = True
        return True
    else:
        return False

if __name__ == '__main__':
    from timeit import Timer
    from sys import argv, exit
    import numpy as np
    if (len(argv) < 2):
        exit()
    artists = argv[1:len(argv)]
    setLastSingleSong(10,10)
    for artist in artists:
        arg = argument(artist)
        if (not arg):
            alldata = searchArtist(artist)
            for data in alldata:
                if(data["error"]==0):
                    printdata(data,artist)
                elif(data["error"]==1):
                    printNotFound(artist)
                else:
                    printError(artist, data["error"] )
