Buscador de Datos de Artistas

                                ArtistFinder V1.8

Requisitos:
  Python3

Para Buscar un artista debes ejecutar >>  ./ArtistFinder "Artista 1" "Artista 2" ...... "Artista N"
      Ejemplo: ./ArtistFinder <comentario> "Foo Fighters" "ACDC"

Tambien puedes ejecutar directamente el Archivo de Python con >> python3 main.py "Artista 1" "Artista 2" ...... "Artista N"
      Ejemplo: python3 main.py <comentario> "avenged sevenfold" "acdc"

Dentro de comentario existen las siguientes posibilidades:

  -R: activa la funcion refind()

Para utilizar el buscador de Artistas tienes las funciones
    ->searchArtist(artista)
        Retorna un Diccionario con
          -Año de Fundacion     "founded"
          -Pais de origen       "country"
          -Albums               "albums"
              es un diccionario con una lista de año "year" y titulo "title"
          -singles              "singles"
              es un diccionario con una lista de año "year" y titulo "title"
          -Un link de youtube   "youtube"

    ->setLastSingleSong(años atras de albums,cantidad de Singles):
        Setea los años atras a imprimir de canciones y la cantidad de singles a imprimir
        en el caso que no se utilice esta funcion imprimira todas los albums y singles

    ->refind():
        activa 2 intentos de busquedas adicionales en caso de encontrar un artista con datos vacios

Repositorio: https://gitlab.com/Amantinetti/ArtistFinder
Por: Arturo Mantinetti
